from django.contrib import admin
from django.urls import path

from example import views
from example.views import OtpLogin, CreateOTP

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index, name='index'),
    path('logout/', views.user_logout, name='logout'),
    path('login/', OtpLogin.as_view(), name='login'),
    path('otp/', CreateOTP.as_view(), name='otp'),
    path('delete/', views.delete, name='delete'),
    path('password_change/', views.password_change, name='change_password')
]