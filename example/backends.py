from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from django.core.exceptions import MultipleObjectsReturned
from django.db.models import Q
from .utils import otp_post_authentication
from .models import Otp
from django.http import HttpResponse
from django.contrib.auth.backends import BaseBackend

UserModel = get_user_model()


class OtpAuthBackend(BaseBackend):
    def authenticate(self, request, username=None, otp=None, **kwargs):
        try:
            user = UserModel.objects.get(username__iexact=username)
            otp = Otp.objects.filter(user=user, used=False).latest('created_at')
            if otp:
                otp_post_authentication(user, otp)
                return user

        except MultipleObjectsReturned:
            return User.objects.filter(email=username).order_by('id').first()
