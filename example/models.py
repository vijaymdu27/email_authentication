from django.contrib.auth.models import User
from django.db import models
from datetime import datetime, timezone
from django.conf import settings


class Otp(models.Model):
    otp = models.CharField(max_length=8)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now=True)
    used = models.BooleanField(default=False)

    def __str__(self):
        return self.otp

    def is_valid(self):
        if (datetime.now(timezone.utc) - self.created_at).seconds < 300:
            return True
        return False

