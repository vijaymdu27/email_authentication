from django.contrib.auth import views as auth_views
from django.contrib import messages
from django.contrib.auth import update_session_auth_hash, authenticate, login
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.models import User
from django.shortcuts import render, redirect, HttpResponse
from .forms import OtpLoginform, CreateOTPForm
from django.views.generic import View
from example.utils import get_object_or_None, otp_post_authentication
from random import randrange
from example.models import Otp
from django.contrib.auth import get_user_model



def index(request):
    return render(request, 'index.html')


def user_logout(request):
    return redirect('login')


def password_change(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)
            return HttpResponse("changed successfully")
        else:
            messages.error(request, 'Please correct the error below.')
    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'change_password.html', {
        'form': form
    })


class CreateOTP(View):
    template_name = 'otp_create.html'
    form = CreateOTPForm

    def get(self, request):
        return render(request, template_name='otp_create.html', context={'form': self.form})

    def post(self, request):
        otp_form = self.form(request.POST)
        otp = None
        if otp_form.is_valid():
            username = otp_form.cleaned_data['username']
            print(username)
            user = get_object_or_None(User, username=username)
            print(user)
            if user:
                random_number = randrange(99999, 999999)
                otp = Otp.objects.create(user=user, otp=random_number)
                context = {'otp': otp}
                # otp.otp = Otp.objects.get(pk=1)
                q = Otp.objects.values_list('otp', flat=True).latest('created_at')
                print(q)
                return render(request, template_name='otp.html', context={'q': q})

            else:
                context = {'error': 'provide valid user'}
                return render(request, template_name='otp_create.html', context=context)
        return HttpResponse("last")


class OtpLogin(View):
    form = OtpLoginform

    def get(self, request):
        return render(request, 'otp_login.html', context={'form': self.form})

    def post(self, request):
        username = request.POST.get('username')
        print(username)
        otp = request.POST.get('otp')
        print(otp)
        user = authenticate(username=username, otp=otp)
        print(user)
        if user:
            login(request, user)
            otp_post_authentication(user, otp)
            print(user)
            return render(request, 'index.html')
        else:
            context = {'error': 'otp is invalid'}
            print("none")
            return render(request, 'otp_login.html', context={'form': self.form})


def delete(request):
    s = Otp.objects.all()
    return HttpResponse("done")
