from .models import Otp
from django.shortcuts import _get_queryset


def get_object_or_None(klass, *args, **kwargs):
    querset = _get_queryset(klass)
    try:
        return querset.get(*args, **kwargs)
    except:
        return None


def otp_post_authentication(user, otp):
    otp = Otp.objects.get(user=user, otp=otp)
    otp.used = True
    otp.save()
    return True
