from django import forms
from django.contrib.auth.models import User


class OtpLoginform(forms.Form):
    username = forms.CharField(label='Username')
    otp = forms.IntegerField(min_value=100000, max_value=999999)

    class Meta:
        model = User
        fields = ("username", "otp")


class CreateOTPForm(forms.Form):
    username = forms.CharField(label='username')

